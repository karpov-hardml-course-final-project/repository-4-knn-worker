import os
import json
import numpy as np

from flask import Flask, request, jsonify, abort
import requests

from qasearch.models import MetaSearchModel, SearchModel

stage = os.environ.get("STAGE", "blue")
model_dir = os.environ.get("MODEL_DIR", f"/var/models/{stage}")

print(stage)
print(model_dir)

app = Flask(__name__)

search_model = MetaSearchModel.deserialize(os.path.join(model_dir, "index.pkl"))


@app.route('/search', methods=['POST'])
def matching():
    if not search_model:
        return json.dumps({"status": "FAISS is not initialized!"})

    input_json = request.get_json()
    query = input_json["query"]
    cluster = input_json["cluster"]
    k = input_json["k"]
    try: 
        result = search_model.search(cluster, query, k=k)
    except ValueError as e:
        return json.dumps({"status": str(e)})
    
    return jsonify({"matching": result})

@app.route("/healthcheck")
def health():
    if search_model:
        return "ok"
    else:
        return abort(501)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=4000)